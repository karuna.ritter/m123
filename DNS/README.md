# Wie man einen Windows 2022 DNS Server einrichtet
 
Wenn Ihr nun Den Windows Server richtig installiert habt dann sollte nun dieses Dashboard vor euch auftauchen.   Hier clickt Ihr auf 2 Add roles and features.
![Alt text](1.png)
Wählt Role based aus und geht auf Next.
![Alt text](2.png)
Nun wählten wir den server aus.
![Alt text](3.png)
Wählt den DNS Server aus und geht auf Next
![Alt text](4.png)
und klickt auf Add Feature
![Alt text](5.png)
Hier ist nun die bestätigungs section. Installiert nun den DNS Server.
![Alt text](6.png)
Nun druckt die "Windows R" und tipt eindnsmgmt.msc ein.
![Alt text](7.png)
Jetzt seid Ihr beim DNS Manager, hier klapt erstellen wir. die Forword Zone
![Alt text](8.png)
Wählt Primary zone aus, dann Next.
![Alt text](9.png)
Hier gebt Ihr nun euren (Webseite) Namen ein in meinem Fall SOTA.com
![Alt text](10.png)
Wir erstelle nun eine Zonendatei (wird im Verzeichnis C:\Windows\System32\dns gespeichert) Dann klickt auf Next.
![Alt text](11.png)
Wählt "Do not allow dynamic updates" aus, dann Next.
![Alt text](12.png)
Schliesse die Einrichtung ab indem du auf Finish klickst und du hast die Forward-Lookup-Zone erstellt.
![Alt text](13.png)         
### Reverse Lookup Zone
wir machen nun so zusagen das umgekehrte wir sorgen dafürdas wenn man nach der IP sucht Den Webseiten Namen erhält. Wir drucken rechtsklick auf Reverse looup zone.
![Alt text](15.png)  
Wir wählen IPv4, dann Next
![Alt text](16.png)
Nun Trage die IP Deines Windowsservers auf, diese kannst du im CMD mit dem befehl ipconfig herausfinden. Dann Next
![Alt text](17.png)
Wähle Primary Zone aus und klicke Next.
![Alt text](9.png)
Erstelle erneut eine Zonendatei (wird im Verzeichnis C:\Windows\System32\dns gespeichert) Dann klickt auf Next.
![Alt text](19.png)
Klicke nun auf Finish um die Zone zu erstellen.
![Alt text](20.png)
Jetzt sehen wir wie man die A- und MX-Einträge hinzufügt. Klappe nun die Forword Lookup Zone auf und drucke rechtsklick auf "SOTA.com", dann "New Host (A or AAAA)"
![Alt text](21.png)
jetzt trägt ihr im Namens feld "WEB" ein Die IP und mach ein hacken bei (PTR) und dann auf Add Host.
![Alt text](23.png) 
Dann wieder Rechtsklick auf "SOTA.com" klicke dann auf "New Mail Exchanger(MX)".
![Alt text](21.png)
Jetzt must du im Host domain bereich "mail" eingeben und die web addresse die vorher in der Forword Lookup Zone bestimmt wurde eingeben in meinem fall web.SOTA.com danach klicke auf OK.
![Alt text](25.png)
Rechtsklick nun auf dem Hostnamen und dan auf "Properties"
![Alt text](26.png) 
Navigiere nun zum Forworder und und klicke auf "Edit"
![Alt text](27.png)
In meinem fall war dies 8.8.8.8, klicke auf "OK"
![Alt text](28.png) 
Nun auf "Apply" und "OK"
![Alt text](29.png) 

Wenn du mit all den schritten fertig bist gehst du zu einem Client der im Selben Netzwerk ist und gibtst "nslookup mit der IP oder Dem Web Namen ein im CMD. Wenn dann eine Bestätigung zurückkommt dann hat alles geklapt.
![Alt text](30.png)
