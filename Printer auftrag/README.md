## Printerserver Trubleshooting 
Ich habe versucht auf meinem Bereits aufgesetzttem Windows 2022 DNS Server den Printerserver einzurichten. Hat leider nicht funktioniert, da das Internet nicht verbinden wollte egal welchen netzwerk adpatper man nahm.

Hier ist dennoch eine anleitung die aber nicht von mir ausgeführt werden konnte.

### Schritt 1: VMware Virtual Machine erstellen

Öffne den VMware vSphere Client oder VMware Workstation.
Klicke auf "Neue virtuelle Maschine erstellen".
Wähle den Installations-Typ, z.B. "Typische Konfiguration".
Wähle das Betriebssystem, in diesem Fall "Windows Server".
Gib einen Namen für die virtuelle Maschine ein und wähle den Speicherort aus.
Festlege die Größe der virtuellen Festplatte und konfiguriere die Hardware nach Bedarf.
Füge das ISO-Image von Windows Server 2022 zur Installation hinzu.
Führe die Installation von Windows Server 2022 durch.
### Schritt 2: Windows Server konfigurieren

Wähle während der Installation die entsprechenden Einstellungen für die Region, Tastatur und Passwort.
Führe die Standardinstallation durch.
Melde dich nach Abschluss der Installation bei Windows Server an.
### Schritt 3: Windows Print Server hinzufügen

Navigiere zum Server-Manager.
Klicke auf "Rollen und Features hinzufügen".
Wähle "Druck- und Dokumentendienste" als Rolle aus.
Klicke auf "Weiter" und dann auf "Installieren".
### Schritt 4: Drucker hinzufügen

Öffne den Server-Manager und wähle "Werkzeuge".
Klicke auf "Druckerverwaltung".
Klicke mit der rechten Maustaste auf "Druckserver" und wähle "Drucker hinzufügen".
Befolge den Assistenten zum Hinzufügen von Druckern.
### Schritt 5: Drucker freigeben

Klicke mit der rechten Maustaste auf den installierten Drucker und wähle "Eigenschaften".
Gehe zum Tab "Freigabe" und aktiviere die Option "Diesen Drucker freigeben".
Gib einen Freigabenamen ein und klicke auf "Übernehmen".
### Schritt 6: Drucker testen

Drucke eine Testseite, um sicherzustellen, dass der Drucker ordnungsgemäß eingerichtet ist.
Das sind die grundlegenden Schritte zur Einrichtung eines Windows Print Servers in einer VMware-Umgebung. Beachte, dass je nach den spezifischen Anforderungen deiner Umgebung zusätzliche Konfigurationsschritte erforderlich sein könnten.

