# Samba Server Installieren

In meinem fall war das problem das ich nicht zugriff auf internet hatten

Als erstes benötigst du eine Ubuntu Desktop / Server VM (Virtuel Maschine)


### Schritt 1: Ubuntu-VM einrichten
Installiere Ubuntu auf deiner virtuellen Maschine.
Stelle sicher, dass die VM eine statische IP-Adresse hat, damit du leicht auf sie zugreifen kannst.

### Schritt 2: Aktualisierung des Systems
Öffne ein Terminal und aktualisiere dein System:

>sudo apt update
![Alt text](sudo-apt-uptdate-terminal-output.png)



### Schritt 3: Samba installieren
Installiere Samba mit dem folgenden Befehl:

>sudo apt install samba -y
![Alt text](installing-samba-terminal-output.png)

Verifiziere die insatllation mit:
>whereis samba
![Alt text](whereis-samba-terminal-output-1.png)

Überprüfe nun ob Samba am Laufen ist mit:
>systemctl status smbd
![Alt text](systemctl-status-smbd-terminal-output.png)

### Schritt 4: Samba-Konfiguration
Sichere die Samba-Konfigurationsdatei:

>sudo cp /etc/samba/smb.conf /etc/samba/smb.conf_backup

Öffne die Samba-Konfigurationsdatei in einem Texteditor:

>sudo nano /etc/samba/smb.conf


Füge am Ende der Datei die folgenden Zeilen hinzu (ersetze "YOUR_SHARE_NAME" und "YOUR_USERNAME" durch deine gewünschten Werte):


[YOUR_SHARE_NAME]

   comment = Your Share Description

   path = /path/to/your/share/folder

   read only = no

   browsable = yes

   valid users = YOUR_USERNAME


Speichere und schließe die Datei.

### Schritt 5: Samba-Benutzer hinzufügen
Füge einen Samba-Benutzer hinzu und setze ein Passwort:

>sudo smbpasswd -a YOUR_USERNAME

### Schritt 6: Firewall-Konfiguration
Falls du eine Firewall verwendest, öffne den Samba-Port:

>sudo ufw allow Samba

### Schritt 7: Samba neu starten
Starte den Samba-Dienst neu, um die Änderungen zu übernehmen:

>sudo service smbd restart

### Schritt 8: Zugriff von einem anderen Computer
Öffne den Dateiexplorer auf einem anderen Computer im gleichen Netzwerk und gib die IP-Adresse der Ubuntu-VM ein. Du solltest aufgefordert werden, dich mit dem Samba-Benutzernamen und -Passwort anzumelden.

Das war's! Du solltest nun in der Lage sein, auf deine freigegebenen Dateien von anderen Geräten im Netzwerk zuzugreifen. Beachte, dass dies nur eine grundlegende Konfiguration ist, und du die Einstellungen je nach deinen Anforderungen anpassen kannst.