[TOC]   

# DHCP Configuration
in dieser Dokumentation wird erklärt wie man auf Filius einen **DHCP-Server** aufsetzt und konfigurieren

## DHCP Setup 
Bei der ausgangslage handelt es sich um eine **Stern-Architektur** mit einem **DHCP-Server** und **drei Clients**.

Wir verbinden nun die **Clients** mit dem **Switch** und den **Switch** mit dem **DHCP-Server.**
![Architektur](./images/Screenshot%202023-11-26%20194024.png)

### DHCP Server einrichten
- DHCP Server Icon ankliken
- Die MAC Adresse des Client 3 eintragen.
- Die Statische IP eintragen.

![DHCP-Server](./images/Screenshot%202023-11-26%20195317.png)

### Netzwerk überprfüfen
- Jetzt machen wir einen Test Ping.
- Wir gehen ins cmd 
- 



## Laborübung 1 - DHCP mit Cisco Packet Tracer 

Router-Modus überprüfen:

Nachdem die CLI geöffnet wurde, wurde der Router-Modus überprüft. Das ">"-Zeichen in R1> deutete darauf hin, dass sich der Administrator im User Mode befand.

### Wechseln in den Enabled Mode:

Der Wechsel in den Enabled Mode wurde durchgeführt, indem der Befehl enable eingegeben und anschließend die Enter-Taste gedrückt wurde. Der Wechsel war erfolgreich, was durch das "#" -Symbol in der Befehlszeile (jetzt R1#) angezeigt wurde.

Befehle ausführen und Fragen beantworten:

### Für welches Subnetz ist der DHCP Server aktiv?

Der Befehl show ip dhcp pool wurde verwendet, um das aktive Subnetz für den DHCP-Server zu identifizieren. Das Subnetz XYZ (Beispiel) wurde dabei festgestellt.

### Welche IP-Adressen sind vergeben und an welche MAC-Adressen? (Antwort als Tabelle)

Der Befehl **show ip dhcp binding** wurde ausgeführt.

#R1#show ip dhcp binding
IP address       Client-ID/             Lease expiration    Type
Hardware address
192.168.20.27    0009.7CA6.4CE3           --           Automatic
192.168.20.30    00E0.8F4E.65AA           --           Automatic
192.168.20.28    0001.632C.3508           --           Automatic
192.168.20.29    0050.0F4E.1D82           --           Automatic
192.168.20.31    00D0.BC52.B29B           --           Automatic
192.168.20.32    0007.ECB6.4534           --           Automatic


### In welchem Range vergibt der DHCP-Server IPv4-Adressen?

Informationen zu den vergebenen Adressbereichen wurden aus dem Output von show ip dhcp pool extrahiert.

### Was hat die Konfiguration **ip dhcp excluded-address** zur Folge?

Durch die Ausführung von show running-config wurde überprüft, dass bestimmte IP-Adressen durch die Konfiguration von **ip dhcp excluded-address** vom DHCP-Bereich ausgeschlossen sind.

### Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Diese Information wurde ebenfalls aus dem Output von show ip dhcp pool abgeleitet.


### Aufgabe 2: Beobachtung der DHCP-Lease
DHCP OP-Codes und Adressen

DHCP-Offer OP-Code: Der OP-Code für DHCP-Offer ist 2.

DHCP-Request OP-Code: Der OP-Code für DHCP-Request ist 1.

DHCP-Acknowledge OP-Code: Der OP-Code für DHCP-Acknowledge ist 5.

DHCP-Discover IP-Adresse: Der DHCP-Discover wird normalerweise als Broadcast 
gesendet, daher ist die Ziel-IP-Adresse 255.255.255.255, was eine Broadcast-Adresse darstellt.

DHCP-Discover MAC-Adresse: Der DHCP-Discover wird an die MAC-Adresse FF:FF:FF:FF:FF:FF gesendet, was eine Broadcast-MAC-Adresse ist.


### Zusätzliche Fragen: Warum wird der DHCP-Discover vom Switch auch an PC3 geschickt? 
Der Switch leitet den DHCP-Discover an alle verbundenen Geräte im Broadcast-Domänen weiter, um einen DHCP-Server zu finden. PC3 könnte in diesem Fall als DHCP-Server fungieren.

Gibt es einen DHCP-Relay im Netzwerk? Überprüfen Sie das Feld "Gateway IP Address" im DHCP-Discover PDU. Wenn es nicht leer ist, handelt es sich um einen DHCP-Relay-Agenten, und das Gerät mit dieser IP-Adresse ist der DHCP-Relay.

Finden der IPv4-Adresse des Clients
IPv4-Adresse, die dem Client zugewiesen ist: Suchen Sie nach dem Feld "Your (client) IP Address" in der DHCP-Acknowledge PDU. Dort steht die dem Client 

zugewiesene IPv4-Adresse.
Screenshots
Sie müssen Screenshots von jeder genannten PDU (Discover, Offer, Request, Acknowledge) machen und sicherstellen, dass die relevanten DHCP-Felder sichtbar sind.

### Aufgabe 3: Netzwerk neu konfigurieren
Schritte
Statische IPv4-Adresse für den Server festlegen:

Gehen Sie zum Konfigurationsmenü des Servers unter Config -> FastEthernet0/0 und setzen Sie eine statische IPv4-Adresse außerhalb des DHCP-Bereichs.
Legen Sie das Gateway in Config -> Settings fest.

Screenshots für den Laborbericht:

Screenshot des Konfigurationsmenüs des Servers mit sichtbarer IPv4-Adresse.

Screenshot des Konfigurationsmenüs des Servers mit sichtbarer Gateway-Adresse.

Screenshot des Webbrowsers eines PCs, der die Webseite des Servers zeigt, einschließlich der sichtbaren IPv4-Adresse.


Überprüfen der Erreichbarkeit des Servers von PC1:

Verwenden Sie die Eingabeaufforderung auf PC1: ping 192.168.X.X (ersetzen Sie X.X durch die IP des Servers).
Stellen Sie sicher, dass der Ping erfolgreich ist, um die Erreichbarkeit des Servers zu überprüfen.
Dokumentation der Aufgabe, als ob sie gelöst wäre:

Beschreiben Sie jeden Schritt ausführlich.
Liefern Sie Screenshots für jede Konfigurationsänderung.
Erklären Sie die Ergebnisse des Ping-Tests.