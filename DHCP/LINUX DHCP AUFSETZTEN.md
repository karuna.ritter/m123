## Einen DHCP Server in Linux einrichten
### Als erstes habe ich die notwendigkeiten eingetragen 

**$ sudo apt update**
![M123](./images/linus%20server%201.png)

**$ sudo apt install isc-dhcp-server -y**
![M123](./images/linux%20server%202.png)

jetzt wo das witchige installiert wurde gehen wir weiter zum...
### Den DHCP Configurieren

Edit **sudo nano /etc/default/isc-dhcp-server** und man muss das ineterface erwähnen welches mann verwendet in meinem Fall waren es ens33

$ sudo nano /etc/default/isc-dhcp-server

INTERFACESv4="enp2s1"

dann wäre der nächste schritt dies abspeichern mit Strg/Cntrl x -> y -> enter

### Editieren der DHCP Configuration

nun geben wir **sudo nano /etc/dhpc/dhcpd.conf**

und löschen diese zeilen code weg:

**#option domain-name "example.org";
#option domain-name-servers ns1.example.org, ns2.example.org;**

**authoritative;**

Danach änderst du die Subnetz deklaration zu:

**subnet 192.168.56.0 netmask 255.255.255.0 {
range 192.168.56.20 192.168.56.120;
  option routers 192.168.56.4;
}**

nun kommt wieder der speicherprozess ctrl/strg x -> y -> enter

nacher kamm leider dieses problem und ich kamm nicht mehr wieter, 

mir wurde angewiessen ich soll etwas mit einer Statischen IP machen.

![M123](./images/linux%20server%203.png)